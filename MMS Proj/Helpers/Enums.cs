﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_Proj.Helpers
{
    public enum StateType { SafeState, UnsafeState}
    public enum FilterType { ColorFilter, InvertFilter, ReverseColorFilter, MeanRemoval, EdgeDetectHomogenity, TimeWarp, GrayScale, BackFromGray, OrderDithering, BurkesDithering, CrossDomain }
    public enum ActionType { Transition, Filter }
    public enum PageType { SingleView, FourView, HistogramView}

    public static class Global
    {
        public static StateType ProgramState { get; set; }
    }
}
