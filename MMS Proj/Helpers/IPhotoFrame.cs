﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_Proj.Helpers
{
    interface IPhotoFrame
    {
        void SetMainPhoto(Image image);
        void FilterImage(FilterType filterType);
        Image GetMainImage();
    }
}
