﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_Proj.Helpers
{
    public static class Constants
    {
        public const int RED_COLOR_VALUE = 52;
        public const int GREEN_COLOR_VALUE = 36;
        public const int BLUE_COLOR_VALUE = -20;
        public const int MAX_ITEMS_UNDOREDO = 20;
        public const int MEAN_REMOVAL_VALUE = 5;
        public const byte EDGE_DETECT_VALUE = 10;
        public const byte TIME_WRAP_VALUE = 15;
        public const int NUMBER_OF_GREY_SHADES = 50;
        public const int T_VALUE_HISTOGRAM = 68;
        public const int C_VALUE_HISTOGRAM = 20;
        public const int CROSS_NEW_HUE = 3;
        public const double CROSS_NEW_SATURATION = 0.8;
    }
}
