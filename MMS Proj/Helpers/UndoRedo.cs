﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ActionMMS = MMS_Proj.Model.ActionMMS;

namespace MMS_Proj.Helpers
{
    public class UndoRedo
    {
        private static List<ActionMMS> _undoStack = new List<ActionMMS>();
        private static List<ActionMMS> _redoStack = new List<ActionMMS>();

        public static void PushToStack(ActionMMS action)
        {
            if(_undoStack.Count >= Constants.MAX_ITEMS_UNDOREDO)
                _undoStack.RemoveAt(0);
            _undoStack.Add(action);
            _redoStack = new List<ActionMMS>();
        }

        public static void ClearStacks()
        {
            _undoStack = new List<ActionMMS>();
            _redoStack = new List<ActionMMS>();
        }

        public static void Undo()
        {
            if (_undoStack.Count == 0)
            {
                return;
            }

            var action = PopUndo();
            _redoStack.Add(action);
            action.SetOdlImage();
        }

        public static void Redo()
        {
            if (_redoStack.Count == 0)
            {
                return;
            }

            var action = PopRedo();
            _undoStack.Add(action);
            action.DoDoneFuntion();
        }

        private static ActionMMS PopUndo()
        {
            var action = _undoStack[_undoStack.Count - 1];
            _undoStack.RemoveAt(_undoStack.Count-1);
            return action;
        }

        private static ActionMMS PopRedo()
        {
            var action = _redoStack[_redoStack.Count - 1];
            _redoStack.RemoveAt(_redoStack.Count - 1);
            return action;

        }
    }
}
