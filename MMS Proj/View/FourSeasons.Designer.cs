﻿namespace MMS_Proj.View
{
    partial class FourSeasons
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainPbox = new System.Windows.Forms.PictureBox();
            this.bPbox = new System.Windows.Forms.PictureBox();
            this.gPbox = new System.Windows.Forms.PictureBox();
            this.rPbox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.mainPbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bPbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gPbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rPbox)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPbox
            // 
            this.mainPbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPbox.Location = new System.Drawing.Point(3, 3);
            this.mainPbox.Name = "mainPbox";
            this.mainPbox.Size = new System.Drawing.Size(194, 171);
            this.mainPbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mainPbox.TabIndex = 0;
            this.mainPbox.TabStop = false;
            // 
            // bPbox
            // 
            this.bPbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bPbox.Location = new System.Drawing.Point(203, 180);
            this.bPbox.Name = "bPbox";
            this.bPbox.Size = new System.Drawing.Size(194, 172);
            this.bPbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bPbox.TabIndex = 1;
            this.bPbox.TabStop = false;
            // 
            // gPbox
            // 
            this.gPbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gPbox.Location = new System.Drawing.Point(3, 180);
            this.gPbox.Name = "gPbox";
            this.gPbox.Size = new System.Drawing.Size(194, 172);
            this.gPbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gPbox.TabIndex = 2;
            this.gPbox.TabStop = false;
            // 
            // rPbox
            // 
            this.rPbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rPbox.Location = new System.Drawing.Point(203, 3);
            this.rPbox.Name = "rPbox";
            this.rPbox.Size = new System.Drawing.Size(194, 171);
            this.rPbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.rPbox.TabIndex = 3;
            this.rPbox.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.bPbox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.gPbox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.mainPbox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rPbox, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(400, 355);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // FourSeasons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FourSeasons";
            this.Size = new System.Drawing.Size(404, 362);
            this.Load += new System.EventHandler(this.FourSeasons_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainPbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bPbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gPbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rPbox)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox mainPbox;
        private System.Windows.Forms.PictureBox bPbox;
        private System.Windows.Forms.PictureBox gPbox;
        private System.Windows.Forms.PictureBox rPbox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
