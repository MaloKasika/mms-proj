﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MMS_Proj.Controller;
using MMS_Proj.Helpers;
using MMS_Proj.Model;

namespace MMS_Proj.View
{
    public partial class SingleSeason : UserControl, IPhotoFrame
    {
        public PictureBox Picture { get; set; }

        public SingleSeason()
        {
            InitializeComponent();
        }

        private void SingleSeason_Load(object sender, EventArgs e)
        {
            Picture = singlePbox;
        }

        public void SetMainPhoto(Image image)
        {
            singlePbox.Image = image;
        }

        public void FilterImage(FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.ColorFilter:
                    singlePbox.Image = Global.ProgramState == StateType.SafeState
                        ? Filters.ColorFilter(singlePbox.Image, Constants.RED_COLOR_VALUE, Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE)
                        : Filters.UnsafeColorFilter(singlePbox.Image, Constants.RED_COLOR_VALUE, Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                    break;
                case FilterType.ReverseColorFilter:
                    singlePbox.Image = Global.ProgramState == StateType.SafeState
                        ? Filters.ColorFilter(singlePbox.Image, -Constants.RED_COLOR_VALUE, -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE)
                        : Filters.UnsafeColorFilter(singlePbox.Image, -Constants.RED_COLOR_VALUE, -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                    break;
                case FilterType.InvertFilter:
                    singlePbox.Image = Global.ProgramState == StateType.SafeState
                        ? Filters.InvertFilter(singlePbox.Image)
                        : Filters.UnsafeInvertFilter(singlePbox.Image);
                    break;
                case FilterType.MeanRemoval:
                    singlePbox.Image = Filters.ConvulationFilter5X5(singlePbox.Image, new MeanRemovalMatrix(Constants.MEAN_REMOVAL_VALUE));
                    break;
                case FilterType.EdgeDetectHomogenity:
                    singlePbox.Image = Filters.EdgeDetectHomogenityFilter(singlePbox.Image, Constants.EDGE_DETECT_VALUE);
                    break;
                case FilterType.TimeWarp:
                    singlePbox.Image = Filters.TimeWarp(singlePbox.Image, Constants.TIME_WRAP_VALUE);
                    break;
                case FilterType.GrayScale:
                    singlePbox.Image = Filters.MaxGrayScale(singlePbox.Image);
                    break;
                case FilterType.BackFromGray:
                    singlePbox.Image = Filters.BackFromGrayScale(singlePbox.Image);
                    break;
                case FilterType.OrderDithering:
                    singlePbox.Image = Filters.OrderDithering(singlePbox.Image);
                    break;
                case FilterType.BurkesDithering:
                    singlePbox.Image = Filters.BurkesDithering(singlePbox.Image);
                    break;
                case FilterType.CrossDomain:
                    singlePbox.Image = Filters.CrossDomainColorize(singlePbox.Image, Constants.CROSS_NEW_HUE, Constants.CROSS_NEW_SATURATION);
                    break;
                    
            }
        }

        public Image GetMainImage()
        {
            return singlePbox.Image;
        }
    }
}
