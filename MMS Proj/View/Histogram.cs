﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MMS_Proj.Controller;
using MMS_Proj.Helpers;
using MMS_Proj.Model;

namespace MMS_Proj.View
{
    public partial class Histogram : UserControl, IPhotoFrame
    {
        ColorFormat filter;
        private CIEValueList ListForHisto;
        private int[] cComponents;
        private int[] iComponents;
        private int[] eComponents;

        public Histogram()
        {
            InitializeComponent();
            filter = new ColorFormat();
            ListForHisto = new CIEValueList();
        }

        public void SetMainPhoto(Image image)
        {
            if (Global.ProgramState == StateType.SafeState)
            {
                mainPhoto.Image = filter.GetRGBComponetns(image);
            }
            else
            {
                mainPhoto.Image = filter.ConvertRBGToCIEUnsafe(image);
            }

            ListForHisto = filter.GetCIEValuesFromImage(mainPhoto.Image);
            cComponents = ListForHisto.GetAllCComponents();
            iComponents = ListForHisto.GetAllIComponents();
            eComponents = ListForHisto.GetAllEComponents();

            PopulateCharts();
        }

        private void PopulateCharts()
        {
            for (int i = 0; i < cComponents.Length; i++)
            {
                CHistogram.Series["C Points"].Points.AddXY(i, cComponents[i]);
            }

            for (int i = 0; i < iComponents.Length; i++)
            {
                IHistogram.Series["I Points"].Points.AddXY(i, iComponents[i]);
            }

            for (int i = 0; i < eComponents.Length; i++)
            {
                EHistogram.Series["E Points"].Points.AddXY(i, eComponents[i]);
            }

        }

        public void FilterImage(FilterType filterType)
        {
            throw new NotImplementedException();
        }

        public Image GetMainImage()
        {
            throw new NotImplementedException();
        }
    }
}
