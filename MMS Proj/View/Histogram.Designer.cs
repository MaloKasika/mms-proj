﻿namespace MMS_Proj.View
{
    partial class Histogram
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.CHistogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.IHistogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.EHistogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.mainPhoto = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.CHistogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IHistogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EHistogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // CHistogram
            // 
            chartArea1.Name = "ChartArea1";
            this.CHistogram.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.CHistogram.Legends.Add(legend1);
            this.CHistogram.Location = new System.Drawing.Point(382, 3);
            this.CHistogram.Name = "CHistogram";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Lime;
            series1.Legend = "Legend1";
            series1.Name = "C Points";
            this.CHistogram.Series.Add(series1);
            this.CHistogram.Size = new System.Drawing.Size(376, 217);
            this.CHistogram.TabIndex = 0;
            this.CHistogram.Text = "chart1";
            // 
            // IHistogram
            // 
            chartArea2.Name = "ChartArea1";
            this.IHistogram.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.IHistogram.Legends.Add(legend2);
            this.IHistogram.Location = new System.Drawing.Point(0, 224);
            this.IHistogram.Name = "IHistogram";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "I Points";
            this.IHistogram.Series.Add(series2);
            this.IHistogram.Size = new System.Drawing.Size(376, 217);
            this.IHistogram.TabIndex = 1;
            this.IHistogram.Text = "chart1";
            // 
            // EHistogram
            // 
            chartArea3.Name = "ChartArea1";
            this.EHistogram.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.EHistogram.Legends.Add(legend3);
            this.EHistogram.Location = new System.Drawing.Point(382, 224);
            this.EHistogram.Name = "EHistogram";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Red;
            series3.Legend = "Legend1";
            series3.Name = "E Points";
            series3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Chocolate;
            this.EHistogram.Series.Add(series3);
            this.EHistogram.Size = new System.Drawing.Size(376, 217);
            this.EHistogram.TabIndex = 2;
            this.EHistogram.Text = "chart1";
            // 
            // mainPhoto
            // 
            this.mainPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPhoto.Location = new System.Drawing.Point(3, 3);
            this.mainPhoto.Name = "mainPhoto";
            this.mainPhoto.Size = new System.Drawing.Size(373, 215);
            this.mainPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mainPhoto.TabIndex = 3;
            this.mainPhoto.TabStop = false;
            // 
            // Histogram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainPhoto);
            this.Controls.Add(this.EHistogram);
            this.Controls.Add(this.IHistogram);
            this.Controls.Add(this.CHistogram);
            this.Name = "Histogram";
            this.Size = new System.Drawing.Size(767, 441);
            ((System.ComponentModel.ISupportInitialize)(this.CHistogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IHistogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EHistogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart CHistogram;
        private System.Windows.Forms.DataVisualization.Charting.Chart IHistogram;
        private System.Windows.Forms.DataVisualization.Charting.Chart EHistogram;
        private System.Windows.Forms.PictureBox mainPhoto;
    }
}
