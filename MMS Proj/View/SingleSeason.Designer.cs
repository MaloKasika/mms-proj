﻿namespace MMS_Proj.View
{
    partial class SingleSeason
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.singlePbox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.singlePbox)).BeginInit();
            this.SuspendLayout();
            // 
            // singlePbox
            // 
            this.singlePbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.singlePbox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.singlePbox.Location = new System.Drawing.Point(3, 3);
            this.singlePbox.Name = "singlePbox";
            this.singlePbox.Size = new System.Drawing.Size(371, 379);
            this.singlePbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.singlePbox.TabIndex = 0;
            this.singlePbox.TabStop = false;
            // 
            // SingleSeason
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.singlePbox);
            this.Name = "SingleSeason";
            this.Size = new System.Drawing.Size(377, 385);
            this.Load += new System.EventHandler(this.SingleSeason_Load);
            ((System.ComponentModel.ISupportInitialize)(this.singlePbox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox singlePbox;
    }
}
