﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MMS_Proj.Controller;
using MMS_Proj.Helpers;
using MMS_Proj.Model;

namespace MMS_Proj.View 
{
    public partial class FourSeasons : UserControl, IPhotoFrame
    {
        public PictureBox MainPicture { get; set; }
        public PictureBox RPicture { get; set; }
        public PictureBox GPicture { get; set; }
        public PictureBox BPicture { get; set; }

        ColorFormat filter;

        public FourSeasons()
        {
            InitializeComponent();
            filter = new ColorFormat();

        }

        private void FourSeasons_Load(object sender, EventArgs e)
        {
            MainPicture = mainPbox;
            RPicture = rPbox;
            GPicture = gPbox;
            BPicture = bPbox;
        }
        
        public void SetMainPhoto(Image image)
        {
            if (Global.ProgramState == StateType.SafeState)
            {
                mainPbox.Image = filter.GetRGBComponetns(image);
                rPbox.Image = filter.GetRChanal(image);
                gPbox.Image = filter.GetGChanal(image);
                bPbox.Image = filter.GetBChanal(image);
            }
            else
            {
                mainPbox.Image = filter.ConvertRBGToCIEUnsafe(image);
                rPbox.Image = filter.GetRChanalUnsafe(image);
                gPbox.Image = filter.GetGChanalUnsafe(image);
                bPbox.Image = filter.GetBChanalUnsafe(image);
            }
        }

        public void FilterImage(FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.ColorFilter:
                    if (Global.ProgramState == StateType.SafeState)
                    {
                        mainPbox.Image = Filters.ColorFilter(mainPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE, Constants.BLUE_COLOR_VALUE);
                        rPbox.Image = Filters.ColorFilter(rPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                        gPbox.Image = Filters.ColorFilter(gPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                        bPbox.Image = Filters.ColorFilter(bPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                    }
                    else
                    {
                        mainPbox.Image = Filters.UnsafeColorFilter(mainPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE, Constants.BLUE_COLOR_VALUE);
                        rPbox.Image = Filters.UnsafeColorFilter(rPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                        gPbox.Image = Filters.UnsafeColorFilter(gPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                        bPbox.Image = Filters.UnsafeColorFilter(bPbox.Image, Constants.RED_COLOR_VALUE,
                            Constants.GREEN_COLOR_VALUE,
                            Constants.BLUE_COLOR_VALUE);
                    }

                    break;
                case FilterType.ReverseColorFilter:
                    if (Global.ProgramState == StateType.SafeState)
                    {
                        mainPbox.Image = Filters.ColorFilter(mainPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE, -Constants.BLUE_COLOR_VALUE);
                        rPbox.Image = Filters.ColorFilter(rPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                        gPbox.Image = Filters.ColorFilter(gPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                        bPbox.Image = Filters.ColorFilter(bPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                    }
                    else
                    {
                        mainPbox.Image = Filters.UnsafeColorFilter(mainPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE, -Constants.BLUE_COLOR_VALUE);
                        rPbox.Image = Filters.UnsafeColorFilter(rPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                        gPbox.Image = Filters.UnsafeColorFilter(gPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                        bPbox.Image = Filters.UnsafeColorFilter(bPbox.Image, -Constants.RED_COLOR_VALUE,
                            -Constants.GREEN_COLOR_VALUE,
                            -Constants.BLUE_COLOR_VALUE);
                    }

                    break;
                case FilterType.InvertFilter:
                    if (Global.ProgramState == StateType.SafeState)
                    {
                        mainPbox.Image = Filters.InvertFilter(mainPbox.Image);
                        rPbox.Image = Filters.InvertFilter(rPbox.Image);
                        gPbox.Image = Filters.InvertFilter(gPbox.Image);
                        bPbox.Image = Filters.InvertFilter(bPbox.Image);
                    }
                    else
                    {
                        mainPbox.Image = Filters.UnsafeInvertFilter(mainPbox.Image);
                        rPbox.Image = Filters.UnsafeInvertFilter(rPbox.Image);
                        gPbox.Image = Filters.UnsafeInvertFilter(gPbox.Image);
                        bPbox.Image = Filters.UnsafeInvertFilter(bPbox.Image);
                    }

                    break;
                case FilterType.MeanRemoval:
                    mainPbox.Image = Filters.ConvulationFilter3X3(mainPbox.Image,
                        new MeanRemovalMatrix(Constants.MEAN_REMOVAL_VALUE));
                    rPbox.Image = Filters.ConvulationFilter3X3(rPbox.Image,
                        new MeanRemovalMatrix(Constants.MEAN_REMOVAL_VALUE));
                    gPbox.Image = Filters.ConvulationFilter5X5(gPbox.Image,
                        new MeanRemovalMatrix(Constants.MEAN_REMOVAL_VALUE));
                    bPbox.Image = Filters.ConvulationFilter7x7(bPbox.Image,
                        new MeanRemovalMatrix(Constants.MEAN_REMOVAL_VALUE));
                    break;
                case FilterType.EdgeDetectHomogenity:
                    mainPbox.Image = Filters.EdgeDetectHomogenityFilter(mainPbox.Image, Constants.EDGE_DETECT_VALUE);
                    rPbox.Image = Filters.EdgeDetectHomogenityFilter(rPbox.Image, Constants.EDGE_DETECT_VALUE);
                    gPbox.Image = Filters.EdgeDetectHomogenityFilter(gPbox.Image, Constants.EDGE_DETECT_VALUE);
                    bPbox.Image = Filters.EdgeDetectHomogenityFilter(bPbox.Image, Constants.EDGE_DETECT_VALUE);
                    break;
                case FilterType.GrayScale:
                    rPbox.Image = Filters.AverageGrayScale(mainPbox.Image);
                    gPbox.Image = Filters.MaxGrayScale(mainPbox.Image);
                    bPbox.Image = Filters.CustomGrayScale(mainPbox.Image);
                    break;
                case FilterType.OrderDithering:
                    mainPbox.Image = Filters.OrderDithering(mainPbox.Image);
                    rPbox.Image = Filters.OrderDithering(rPbox.Image);
                    gPbox.Image = Filters.OrderDithering(gPbox.Image);
                    bPbox.Image = Filters.OrderDithering(bPbox.Image);
                    break;
                case FilterType.BurkesDithering:
                    mainPbox.Image = Filters.BurkesDithering(mainPbox.Image);
                    rPbox.Image = Filters.BurkesDithering(rPbox.Image);
                    gPbox.Image = Filters.BurkesDithering(gPbox.Image);
                    bPbox.Image = Filters.BurkesDithering(bPbox.Image);
                    break;
                case FilterType.CrossDomain:
                    mainPbox.Image = Filters.CrossDomainColorize(mainPbox.Image, Constants.CROSS_NEW_HUE, Constants.CROSS_NEW_SATURATION);
                    rPbox.Image = Filters.CrossDomainColorize(rPbox.Image, Constants.CROSS_NEW_HUE, Constants.CROSS_NEW_SATURATION);
                    gPbox.Image = Filters.CrossDomainColorize(gPbox.Image, Constants.CROSS_NEW_HUE, Constants.CROSS_NEW_SATURATION);
                    bPbox.Image = Filters.CrossDomainColorize(bPbox.Image, Constants.CROSS_NEW_HUE, Constants.CROSS_NEW_SATURATION);
                    break;


            }
        }

        public Image GetMainImage()
        {
            return mainPbox.Image;
        }
    }
}
