﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS_Proj.Helpers;

namespace MMS_Proj.Model
{
    public class CIEValueList
    {
        private List<CIEColorFormat> _listOfColors;

        public List<CIEColorFormat> ListOfColors
        {
            get => _listOfColors;
            set => _listOfColors = value;
        }

        public CIEValueList()
        {
            _listOfColors = new List<CIEColorFormat>();
        }

        public int[] GetAllCComponents()
        {
            var cList = new List<int>();

            foreach (var color in _listOfColors)
            {
                cList.Add(color.CComponent);
            }

            var cArray = cList.ToArray();
            var maxC = cArray.Max();
            var finalCArray = new int[maxC+1 - Constants.C_VALUE_HISTOGRAM];

            int indexValue;
            foreach (var cValue in cArray)
            {
                if (cValue < Constants.T_VALUE_HISTOGRAM)
                {
                    indexValue = 0;
                }
                else
                {
                    indexValue = cValue - Constants.C_VALUE_HISTOGRAM;
                }

                finalCArray[indexValue]++;
            }

            return finalCArray;
        }

        public int[] GetAllIComponents()
        {
            var iList = new List<int>();

            foreach (var color in _listOfColors)
            {
                iList.Add(color.IComponent);
            }

            var iArray = iList.ToArray();
            var maxI = iArray.Max();
            var finalIArray = new int[maxI + 1 - Constants.C_VALUE_HISTOGRAM];

            int indexValue;
            foreach (var iValue in iArray)
            {
                if (iValue < Constants.T_VALUE_HISTOGRAM)
                {
                    indexValue = 0;
                }
                else
                {
                    indexValue = iValue - Constants.C_VALUE_HISTOGRAM;
                }

                finalIArray[indexValue]++;
            }

            return finalIArray;
        }

        public int[] GetAllEComponents()
        {
            var eList = new List<int>();

            foreach (var color in _listOfColors)
            {
                eList.Add(color.EComponent);
            }

            var eArray = eList.ToArray();
            var maxE = eArray.Max();
            var finalEArray = new int[maxE + 1 - Constants.C_VALUE_HISTOGRAM];
        
            int indexValue;
            foreach (var eValue in eArray)
            {
                if (eValue < Constants.T_VALUE_HISTOGRAM)
                {
                    indexValue = 0;
                }
                else
                {
                    indexValue = eValue - Constants.C_VALUE_HISTOGRAM;
                }

                finalEArray[indexValue]++;
            }

            return finalEArray;
        }
    }
}
