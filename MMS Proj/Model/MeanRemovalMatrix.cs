﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_Proj.Model
{
    public class MeanRemovalMatrix : ConvulationMatrix
    {
        public MeanRemovalMatrix(int value)
        {
            SetAll(-1);
            Pixel = value;
            Factor = value - 8;
        }
    }
}
