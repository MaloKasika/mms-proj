﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MMS_Proj.Helpers;

namespace MMS_Proj.Model
{
    public class ActionMMS
    {
        public ActionType ActionType { get; set; }
        public event EventHandler DoneFuntion;
        public event EventHandler OldImage;


        public void DoDoneFuntion()
        {
            DoneFuntion?.Invoke(this, EventArgs.Empty);
        }

        public void SetOdlImage()
        {
            if (ActionType == ActionType.Transition)
            {
                DoneFuntion?.Invoke(this, EventArgs.Empty);
                return;
            }

            OldImage?.Invoke(this, EventArgs.Empty);
        }
    }
}
