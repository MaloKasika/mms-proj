﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_Proj.Model
{
    public class CIEColorFormat
    {
        private int _cComponent;
        private int _iComponent;
        private int _eComponent;

        public int CComponent
        {
            get => _cComponent;
            set => _cComponent = value;
        }

        public int IComponent
        {
            get => _iComponent;
            set => _iComponent = value;
        }

        public int EComponent
        {
            get => _eComponent;
            set => _eComponent = value;
        }
    }
}
