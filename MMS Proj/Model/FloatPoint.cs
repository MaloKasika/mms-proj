﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_Proj.Model
{
    public struct FloatPoint
    {
        public double X;
        public double Y;
    }
}
