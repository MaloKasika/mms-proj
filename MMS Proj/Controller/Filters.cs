﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS_Proj.Helpers;
using MMS_Proj.Model;

namespace MMS_Proj.Controller
{
    public class Filters
    {
        public static Color[] GrayPaletteArray;

        public static void SetValueTGrayArray(int i, Color value)
        {
            if (GrayPaletteArray == null)
            {
                GrayPaletteArray = new Color[256];
            }

            if (GrayPaletteArray[i] == Color.Empty)
            {
                GrayPaletteArray[i] = value;

            }
        }

        public static Image ColorFilter(Image image, int rValue, int gValue, int bValue)
        {
            var bitmap = new Bitmap(image);

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    var newR = ((pix.R + rValue) > 255) ? 255 : pix.R + rValue;
                    var newG = ((pix.G + gValue) > 255) ? 255 : pix.G + gValue;
                    var newB = ((pix.B + bValue) > 255) ? 255 : pix.B + bValue;
                    newR = (newR < 0) ? 0 : newR;
                    newG = (newG < 0) ? 0 : newG;
                    newB = (newB < 0) ? 0 : newB;
                    bitmap.SetPixel(i, j, Color.FromArgb(newR, newG, newB));
                }
            }

            return bitmap;
        }

        public static Image UnsafeColorFilter(Image image, int rValue, int gValue, int bValue)
        {
            if (rValue < -255 || rValue > 255) return null;
            if (gValue < -255 || gValue > 255) return null;
            if (bValue < -255 || bValue > 255) return null;

            var bitmap = new Bitmap(image);
            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - bitmap.Width * 3;
                int nPixel;

                for (int y = 0; y < bitmap.Height; ++y)
                {
                    for (int x = 0; x < bitmap.Width; ++x)
                    {
                        nPixel = p[2] + rValue;
                        nPixel = Math.Max(nPixel, 0);
                        p[2] = (byte) Math.Min(255, nPixel);

                        nPixel = p[1] + gValue;
                        nPixel = Math.Max(nPixel, 0);
                        p[1] = (byte) Math.Min(255, nPixel);

                        nPixel = p[0] + bValue;
                        nPixel = Math.Max(nPixel, 0);
                        p[0] = (byte) Math.Min(255, nPixel);

                        p += 3;
                    }

                    p += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);

            return bitmap;
        }

        public static Image InvertFilter(Image image)
        {
            var bitmap = new Bitmap(image);

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    var newR = ((255 - pix.R) < 0) ? 0 : (255 - pix.R);
                    var newG = ((255 - pix.G) < 0) ? 0 : (255 - pix.G);
                    var newB = ((255 - pix.B) < 0) ? 0 : (255 - pix.B);
                    bitmap.SetPixel(i, j, Color.FromArgb(newR, newG, newB));
                }
            }

            return bitmap;
        }

        public static Image UnsafeInvertFilter(Image image)
        {
            var bitmap = new Bitmap(image);
            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - bitmap.Width * 3;
                int nWidth = bitmap.Width * 3;

                for (int y = 0; y < bitmap.Height; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        p[0] = (byte) (255 - p[0]);
                        ++p;
                    }

                    p += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);

            return bitmap;
        }

        public static Image ConvulationFilter3X3(Image image, ConvulationMatrix matrix)
        {
            var bitmap = new Bitmap(image);
            var secondBitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmNew = secondBitmap.LockBits(new Rectangle(0, 0, secondBitmap.Width, secondBitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmNew.Scan0;
            unsafe
            {
                byte* p = (byte*) (void*) SrcScan0;
                byte* pSrc = (byte*) (void*) Scan0;

                int nOffset = stride - bitmap.Width * 3;
                int nWidth = bitmap.Width - 2;
                int nHeight = bitmap.Height - 2;

        
                int nPixel;
                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((((pSrc[2] * matrix.TopLeft) + (pSrc[5] * matrix.TopMid) +
                                    (pSrc[8] * matrix.TopRight) +
                                    (pSrc[2 + stride] * matrix.MidLeft) + (pSrc[5 + stride] * matrix.Pixel) +
                                    (pSrc[8 + stride] * matrix.MidRight) +
                                    (pSrc[2 + stride2] * matrix.BottomLeft) + (pSrc[5 + stride2] * matrix.BottomMid) +
                                    (pSrc[8 + stride2] * matrix.BottomRight)) / matrix.Factor) + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[5 + stride] = (byte) nPixel;

                        nPixel = ((((pSrc[1] * matrix.TopLeft) + (pSrc[4] * matrix.TopMid) +
                                    (pSrc[7] * matrix.TopRight) +
                                    (pSrc[1 + stride] * matrix.MidLeft) + (pSrc[4 + stride] * matrix.Pixel) +
                                    (pSrc[7 + stride] * matrix.MidRight) +
                                    (pSrc[1 + stride2] * matrix.BottomLeft) + (pSrc[4 + stride2] * matrix.BottomMid) +
                                    (pSrc[7 + stride2] * matrix.BottomRight)) / matrix.Factor) + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[4 + stride] = (byte) nPixel;

                        nPixel = ((((pSrc[0] * matrix.TopLeft) + (pSrc[3] * matrix.TopMid) +
                                    (pSrc[6] * matrix.TopRight) +
                                    (pSrc[0 + stride] * matrix.MidLeft) + (pSrc[3 + stride] * matrix.Pixel) +
                                    (pSrc[6 + stride] * matrix.MidRight) +
                                    (pSrc[0 + stride2] * matrix.BottomLeft) + (pSrc[3 + stride2] * matrix.BottomMid) +
                                    (pSrc[6 + stride2] * matrix.BottomRight)) / matrix.Factor) + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[3 + stride] = (byte) nPixel;

                        p += 3;
                        pSrc += 3;
                    }

                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);
            secondBitmap.UnlockBits(bmNew);

            return secondBitmap;
        }

        public static Image ConvulationFilter5X5(Image image, ConvulationMatrix matrix)
        {
            var bitmap = new Bitmap(image);
            var secondBitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmNew = secondBitmap.LockBits(new Rectangle(0, 0, secondBitmap.Width, secondBitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            int stride3 = stride * 3;
            int stride4 = stride * 4;

            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmNew.Scan0;
            unsafe
            {
                byte* p = (byte*) (void*) SrcScan0;
                byte* pSrc = (byte*) (void*) Scan0;

                int nOffset = stride - bitmap.Width * 3;
                int nWidth = bitmap.Width - 2;
                int nHeight = bitmap.Height - 2;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((pSrc[2] * matrix.TopLeft + pSrc[5] * matrix.TopLeft + pSrc[8] * matrix.TopMid +
                                   pSrc[11] * matrix.TopMid + pSrc[14] * matrix.TopRight +
                                   pSrc[2 + stride] * matrix.MidLeft + pSrc[5 + stride] * matrix.TopLeft +
                                   pSrc[8 + stride] * matrix.TopMid + pSrc[11 + stride] * matrix.TopLeft +
                                   pSrc[14 + stride] * matrix.TopLeft +
                                   pSrc[2 + stride2] * matrix.MidLeft + pSrc[5 + stride2] * matrix.MidLeft +
                                   pSrc[8 + stride2] * matrix.Pixel + pSrc[11 + stride2] * matrix.MidRight +
                                   pSrc[14 + stride2] * matrix.MidRight +
                                   pSrc[2 + stride3] * matrix.BottomLeft + pSrc[5 + stride3] * matrix.BottomLeft +
                                   pSrc[8 + stride3] * matrix.BottomMid + pSrc[11 + stride3] * matrix.BottomRight +
                                   pSrc[14 + stride3] * matrix.MidRight +
                                   pSrc[2 + stride4] * matrix.BottomLeft + pSrc[5 + stride4] * matrix.BottomMid +
                                   pSrc[8 + stride4] * matrix.BottomMid + pSrc[11 + stride4] * matrix.BottomRight +
                                   pSrc[14 + stride4] * matrix.BottomRight) / matrix.Factor + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[8 + stride2] = (byte) nPixel;

                        nPixel = ((pSrc[1] * matrix.TopLeft + pSrc[4] * matrix.TopLeft + pSrc[7] * matrix.TopMid +
                                   pSrc[10] * matrix.TopMid + pSrc[13] * matrix.TopRight +
                                   pSrc[1 + stride] * matrix.MidLeft + pSrc[4 + stride] * matrix.TopLeft +
                                   pSrc[7 + stride] * matrix.TopMid + pSrc[10 + stride] * matrix.TopLeft +
                                   pSrc[13 + stride] * matrix.TopLeft +
                                   pSrc[1 + stride2] * matrix.MidLeft + pSrc[4 + stride2] * matrix.MidLeft +
                                   pSrc[7 + stride2] * matrix.Pixel + pSrc[10 + stride2] * matrix.MidRight +
                                   pSrc[13 + stride2] * matrix.MidRight +
                                   pSrc[1 + stride3] * matrix.BottomLeft + pSrc[4 + stride3] * matrix.BottomLeft +
                                   pSrc[7 + stride3] * matrix.BottomMid + pSrc[10 + stride3] * matrix.BottomRight +
                                   pSrc[13 + stride3] * matrix.MidRight +
                                   pSrc[1 + stride4] * matrix.BottomLeft + pSrc[4 + stride4] * matrix.BottomMid +
                                   pSrc[7 + stride4] * matrix.BottomMid + pSrc[10 + stride4] * matrix.BottomRight +
                                   pSrc[13 + stride4] * matrix.BottomRight) / matrix.Factor + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[7 + stride2] = (byte) nPixel;

                        nPixel = ((pSrc[0] * matrix.TopLeft + pSrc[3] * matrix.TopLeft + pSrc[6] * matrix.TopMid +
                                   pSrc[9] * matrix.TopMid + pSrc[12] * matrix.TopRight +
                                   pSrc[0 + stride] * matrix.MidLeft + pSrc[3 + stride] * matrix.TopLeft +
                                   pSrc[6 + stride] * matrix.TopMid + pSrc[9 + stride] * matrix.TopLeft +
                                   pSrc[12 + stride] * matrix.TopLeft +
                                   pSrc[0 + stride2] * matrix.MidLeft + pSrc[3 + stride2] * matrix.MidLeft +
                                   pSrc[6 + stride2] * matrix.Pixel + pSrc[9 + stride2] * matrix.MidRight +
                                   pSrc[12 + stride2] * matrix.MidRight +
                                   pSrc[0 + stride3] * matrix.BottomLeft + pSrc[3 + stride3] * matrix.BottomLeft +
                                   pSrc[6 + stride3] * matrix.BottomMid + pSrc[9 + stride3] * matrix.BottomRight +
                                   pSrc[12 + stride3] * matrix.MidRight +
                                   pSrc[0 + stride4] * matrix.BottomLeft + pSrc[3 + stride4] * matrix.BottomMid +
                                   pSrc[6 + stride4] * matrix.BottomMid + pSrc[9 + stride4] * matrix.BottomRight +
                                   pSrc[12 + stride4] * matrix.BottomRight) / matrix.Factor + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[6 + stride2] = (byte) nPixel;

                        p += 3;
                        pSrc += 3;
                    }

                    p += nOffset;
                    pSrc += nOffset;
                }

            }

            bitmap.UnlockBits(bmData);
            secondBitmap.UnlockBits(bmNew);

            return secondBitmap;

        }

        public static Image ConvulationFilter7x7(Image image, ConvulationMatrix matrix)
        {
            var bitmap = new Bitmap(image);
            var secondBitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmNew = secondBitmap.LockBits(new Rectangle(0, 0, secondBitmap.Width, secondBitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            int stride3 = stride * 3;
            int stride4 = stride * 4;
            int stride5 = stride * 5;
            int stride6 = stride * 6;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmNew.Scan0;

            unsafe
            {
                byte* p = (byte*) (void*) Scan0;
                byte* pSrc = (byte*) (void*) SrcScan0;

                int nOffset = stride - bitmap.Width * 3;
                int nWidth = bitmap.Width - 5;
                int nHeight = bitmap.Height - 5;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((
                                      pSrc[2] * matrix.TopLeft + pSrc[5] * matrix.TopLeft + pSrc[8] * 0 +
                                      pSrc[11] * matrix.TopMid + pSrc[14] * matrix.TopMid + pSrc[17] * 0 +
                                      pSrc[20] * matrix.TopRight +
                                      pSrc[2 + stride] * 0 + pSrc[5 + stride] * matrix.TopLeft +
                                      pSrc[8 + stride] * matrix.TopLeft + pSrc[11 + stride] * matrix.TopMid +
                                      pSrc[14 + stride] * matrix.TopMid + pSrc[17 + stride] * matrix.TopRight +
                                      pSrc[20 + stride] * matrix.TopRight +
                                      pSrc[2 + stride2] * matrix.MidLeft + pSrc[5 + stride2] * matrix.MidLeft +
                                      pSrc[8 + stride2] * matrix.TopLeft + pSrc[11 + stride2] * matrix.TopMid +
                                      pSrc[14 + stride2] * matrix.TopRight + pSrc[17 + stride2] * matrix.TopRight +
                                      pSrc[20 + stride2] * 0 +
                                      pSrc[2 + stride3] * matrix.MidLeft + pSrc[5 + stride3] * matrix.MidLeft +
                                      pSrc[8 + stride3] * matrix.MidLeft + pSrc[11 + stride3] * matrix.Pixel +
                                      pSrc[14 + stride3] * matrix.MidRight + pSrc[17 + stride3] * matrix.MidRight +
                                      pSrc[20 + stride3] * matrix.MidRight +
                                      pSrc[2 + stride4] * 0 + pSrc[5 + stride4] * matrix.BottomLeft +
                                      pSrc[8 + stride4] * matrix.BottomLeft + pSrc[11 + stride4] * matrix.BottomMid +
                                      pSrc[14 + stride4] * matrix.BottomRight + pSrc[17 + stride4] * matrix.MidRight +
                                      pSrc[20 + stride4] * matrix.MidRight +
                                      pSrc[2 + stride5] * matrix.BottomLeft + pSrc[5 + stride5] * matrix.BottomLeft +
                                      pSrc[8 + stride5] * matrix.BottomMid + pSrc[11 + stride5] * matrix.BottomMid +
                                      pSrc[14 + stride5] * matrix.BottomRight +
                                      pSrc[17 + stride5] * matrix.BottomRight + pSrc[20 + stride5] * 0 +
                                      pSrc[2 + stride6] * matrix.BottomLeft + pSrc[5 + stride6] * 0 +
                                      pSrc[8 + stride6] * matrix.BottomMid + pSrc[11 + stride6] * matrix.BottomMid +
                                      pSrc[14 + stride6] * 0 + pSrc[17 + stride6] * matrix.BottomRight +
                                      pSrc[20 + stride6] * matrix.BottomRight) / matrix.Factor + matrix.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[11 + stride3] = (byte) nPixel;

                        nPixel = ((
                                      pSrc[1] * matrix.TopLeft + pSrc[4] * matrix.TopLeft + pSrc[7] * 0 +
                                      pSrc[10] * matrix.TopMid + pSrc[13] * matrix.TopMid + pSrc[16] * 0 +
                                      pSrc[19] * matrix.TopRight +
                                      pSrc[1 + stride] * 0 + pSrc[4 + stride] * matrix.TopLeft +
                                      pSrc[7 + stride] * matrix.TopLeft + pSrc[10 + stride] * matrix.TopMid +
                                      pSrc[13 + stride] * matrix.TopMid + pSrc[16 + stride] * matrix.TopRight +
                                      pSrc[19 + stride] * matrix.TopRight +
                                      pSrc[1 + stride2] * matrix.MidLeft + pSrc[4 + stride2] * matrix.MidLeft +
                                      pSrc[7 + stride2] * matrix.TopLeft + pSrc[10 + stride2] * matrix.TopMid +
                                      pSrc[13 + stride2] * matrix.TopRight + pSrc[16 + stride2] * matrix.TopRight +
                                      pSrc[19 + stride2] * 0 +
                                      pSrc[1 + stride3] * matrix.MidLeft + pSrc[4 + stride3] * matrix.MidLeft +
                                      pSrc[7 + stride3] * matrix.MidLeft + pSrc[10 + stride3] * matrix.Pixel +
                                      pSrc[13 + stride3] * matrix.MidRight + pSrc[16 + stride3] * matrix.MidRight +
                                      pSrc[19 + stride3] * matrix.MidRight +
                                      pSrc[1 + stride4] * 0 + pSrc[4 + stride4] * matrix.BottomLeft +
                                      pSrc[7 + stride4] * matrix.BottomLeft + pSrc[10 + stride4] * matrix.BottomMid +
                                      pSrc[13 + stride4] * matrix.BottomRight + pSrc[16 + stride4] * matrix.MidRight +
                                      pSrc[19 + stride4] * matrix.MidRight +
                                      pSrc[1 + stride5] * matrix.BottomLeft + pSrc[4 + stride5] * matrix.BottomLeft +
                                      pSrc[7 + stride5] * matrix.BottomMid + pSrc[10 + stride5] * matrix.BottomMid +
                                      pSrc[13 + stride5] * matrix.BottomRight +
                                      pSrc[16 + stride5] * matrix.BottomRight + pSrc[19 + stride5] * 0 +
                                      pSrc[1 + stride6] * matrix.BottomLeft + pSrc[4 + stride6] * 0 +
                                      pSrc[7 + stride6] * matrix.BottomMid + pSrc[10 + stride6] * matrix.BottomMid +
                                      pSrc[13 + stride6] * 0 + pSrc[16 + stride6] * matrix.BottomRight +
                                      pSrc[19 + stride6] * matrix.BottomRight) / matrix.Factor + matrix.Offset);


                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[10 + stride3] = (byte) nPixel;


                        nPixel = ((
                                      pSrc[0] * matrix.TopLeft + pSrc[3] * matrix.TopLeft + pSrc[6] * 0 +
                                      pSrc[9] * matrix.TopMid + pSrc[12] * matrix.TopMid + pSrc[15] * 0 +
                                      pSrc[18] * matrix.TopRight +
                                      pSrc[0 + stride] * 0 + pSrc[3 + stride] * matrix.TopLeft +
                                      pSrc[6 + stride] * matrix.TopLeft + pSrc[9 + stride] * matrix.TopMid +
                                      pSrc[12 + stride] * matrix.TopMid + pSrc[15 + stride] * matrix.TopRight +
                                      pSrc[18 + stride] * matrix.TopRight +
                                      pSrc[0 + stride2] * matrix.MidLeft + pSrc[3 + stride2] * matrix.MidLeft +
                                      pSrc[6 + stride2] * matrix.TopLeft + pSrc[9 + stride2] * matrix.TopMid +
                                      pSrc[12 + stride2] * matrix.TopRight + pSrc[15 + stride2] * matrix.TopRight +
                                      pSrc[18 + stride2] * 0 +
                                      pSrc[0 + stride3] * matrix.MidLeft + pSrc[3 + stride3] * matrix.MidLeft +
                                      pSrc[6 + stride3] * matrix.MidLeft + pSrc[9 + stride3] * matrix.Pixel +
                                      pSrc[12 + stride3] * matrix.MidRight + pSrc[15 + stride3] * matrix.MidRight +
                                      pSrc[18 + stride3] * matrix.MidRight +
                                      pSrc[0 + stride4] * 0 + pSrc[3 + stride4] * matrix.BottomLeft +
                                      pSrc[6 + stride4] * matrix.BottomLeft + pSrc[9 + stride4] * matrix.BottomMid +
                                      pSrc[12 + stride4] * matrix.BottomRight + pSrc[15 + stride4] * matrix.MidRight +
                                      pSrc[18 + stride4] * matrix.MidRight +
                                      pSrc[0 + stride5] * matrix.BottomLeft + pSrc[3 + stride5] * matrix.BottomLeft +
                                      pSrc[6 + stride5] * matrix.BottomMid + pSrc[9 + stride5] * matrix.BottomMid +
                                      pSrc[12 + stride5] * matrix.BottomRight +
                                      pSrc[15 + stride5] * matrix.BottomRight + pSrc[18 + stride5] * 0 +
                                      pSrc[0 + stride6] * matrix.BottomLeft + pSrc[3 + stride6] * 0 +
                                      pSrc[6 + stride6] * matrix.BottomMid + pSrc[9 + stride6] * matrix.BottomMid +
                                      pSrc[12 + stride6] * 0 + pSrc[15 + stride6] * matrix.BottomRight +
                                      pSrc[18 + stride6] * matrix.BottomRight) / matrix.Factor + matrix.Offset);


                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[9 + stride3] = (byte) nPixel;

                        p += 3;
                        pSrc += 3;
                    }

                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);
            secondBitmap.UnlockBits(bmNew);

            return secondBitmap;
        }

        public static Image EdgeDetectHomogenityFilter(Image image, byte nThreshold)
        {
            var bitmap = new Bitmap(image);
            var secondBitmap = new Bitmap(image);

            var bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb);
            var bmData2 = secondBitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            var stride = bmData.Stride;
            var Scan0 = bmData.Scan0;
            var Scan02 = bmData2.Scan0;

            unsafe
            {
                byte* p = (byte*) (void*) Scan0;
                byte* p2 = (byte*) (void*) Scan02;

                int nOffset = stride - bitmap.Width * 3;
                int nWidth = bitmap.Width * 3;

                int nPixel = 0, nPixelMax = 0;

                p += stride;
                p2 += stride;

                for (int y = 1; y < bitmap.Height - 1; ++y)
                {
                    p += 3;
                    p2 += 3;

                    for (int x = 3; x < nWidth - 3; ++x)
                    {
                        nPixelMax = Math.Abs(p2[0] - (p2 + stride - 3)[0]);
                        nPixel = Math.Abs(p2[0] - (p2 + stride)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        nPixel = Math.Abs(p2[0] - (p2 + stride + 3)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        nPixel = Math.Abs(p2[0] - (p2 - stride)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        nPixel = Math.Abs(p2[0] - (p2 + stride)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        nPixel = Math.Abs(p2[0] - (p2 - stride - 3)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        nPixel = Math.Abs(p2[0] - (p2 - stride)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        nPixel = Math.Abs(p2[0] - (p2 - stride + 3)[0]);
                        if (nPixel > nPixelMax) nPixelMax = nPixel;

                        if (nPixelMax < nThreshold) nPixelMax = 0;

                        p[0] = (byte) nPixelMax;

                        ++p;
                        ++p2;
                    }

                    p += 3 + nOffset;
                    p2 += 3 + nOffset;
                }
            }

            bitmap.UnlockBits(bmData);
            secondBitmap.UnlockBits(bmData2);

            return bitmap;

        }

        public static Image TimeWarp(Image image, Byte factor)
        {
            var bitmap = new Bitmap(image);

            int nWidth = bitmap.Width;
            int nHeight = bitmap.Height;

            FloatPoint[,] fp = new FloatPoint[nWidth, nHeight];
            Point[,] pt = new Point[nWidth, nHeight];

            Point mid = new Point();
            mid.X = nWidth / 2;
            mid.Y = nHeight / 2;

            double theta, radius;
            double newX, newY;

            for (int x = 0; x < nWidth; ++x)
            for (int y = 0; y < nHeight; ++y)
            {
                int trueX = x - mid.X;
                int trueY = y - mid.Y;
                theta = Math.Atan2((trueY), (trueX));

                radius = Math.Sqrt(trueX * trueX + trueY * trueY);

                double newRadius = Math.Sqrt(radius) * factor;

                newX = mid.X + (newRadius * Math.Cos(theta));
                if (newX > 0 && newX < nWidth)
                {
                    fp[x, y].X = newX;
                    pt[x, y].X = (int) newX;
                }
                else
                {
                    fp[x, y].X = 0.0;
                    pt[x, y].X = 0;
                }

                newY = mid.Y + (newRadius * Math.Sin(theta));
                if (newY > 0 && newY < nHeight)
                {
                    fp[x, y].Y = newY;
                    pt[x, y].Y = (int) newY;
                }
                else
                {
                    fp[x, y].Y = 0.0;
                    pt[x, y].Y = 0;
                }
            }

            return OffsetFilterAntiAlias(bitmap, fp);
        }

        public static Bitmap OffsetFilterAntiAlias(Bitmap b, FloatPoint[,] fp)
        {
            Bitmap bSrc = (Bitmap) b.Clone();

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb);
            BitmapData bmSrc = bSrc.LockBits(new Rectangle(0, 0, bSrc.Width, bSrc.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb);

            int scanline = bmData.Stride;

            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmSrc.Scan0;

            unsafe
            {
                byte* p = (byte*) (void*) Scan0;
                byte* pSrc = (byte*) (void*) SrcScan0;

                int nOffset = bmData.Stride - b.Width * 3;
                int nWidth = b.Width;
                int nHeight = b.Height;

                double xOffset, yOffset;

                double fraction_x, fraction_y, one_minus_x, one_minus_y;
                int ceil_x, ceil_y, floor_x, floor_y;
                Byte p1, p2;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        xOffset = fp[x, y].X;
                        yOffset = fp[x, y].Y;

                        // Setup

                        floor_x = (int) Math.Floor(xOffset);
                        floor_y = (int) Math.Floor(yOffset);
                        ceil_x = floor_x + 1;
                        ceil_y = floor_y + 1;
                        fraction_x = xOffset - floor_x;
                        fraction_y = yOffset - floor_y;
                        one_minus_x = 1.0 - fraction_x;
                        one_minus_y = 1.0 - fraction_y;

                        if (floor_y >= 0 && ceil_y < nHeight && floor_x >= 0 && ceil_x < nWidth)
                        {
                            // Blue

                            p1 = (Byte) (one_minus_x * (double) (pSrc[floor_y * scanline + floor_x * 3]) +
                                         fraction_x * (double) (pSrc[floor_y * scanline + ceil_x * 3]));

                            p2 = (Byte) (one_minus_x * (double) (pSrc[ceil_y * scanline + floor_x * 3]) +
                                         fraction_x * (double) (pSrc[ceil_y * scanline + 3 * ceil_x]));

                            p[x * 3 + y * scanline] = (Byte) (one_minus_y * (double) (p1) + fraction_y * (double) (p2));

                            // Green

                            p1 = (Byte) (one_minus_x * (double) (pSrc[floor_y * scanline + floor_x * 3 + 1]) +
                                         fraction_x * (double) (pSrc[floor_y * scanline + ceil_x * 3 + 1]));

                            p2 = (Byte) (one_minus_x * (double) (pSrc[ceil_y * scanline + floor_x * 3 + 1]) +
                                         fraction_x * (double) (pSrc[ceil_y * scanline + 3 * ceil_x + 1]));

                            p[x * 3 + y * scanline + 1] =
                                (Byte) (one_minus_y * (double) (p1) + fraction_y * (double) (p2));

                            // Red

                            p1 = (Byte) (one_minus_x * (double) (pSrc[floor_y * scanline + floor_x * 3 + 2]) +
                                         fraction_x * (double) (pSrc[floor_y * scanline + ceil_x * 3 + 2]));

                            p2 = (Byte) (one_minus_x * (double) (pSrc[ceil_y * scanline + floor_x * 3 + 2]) +
                                         fraction_x * (double) (pSrc[ceil_y * scanline + 3 * ceil_x + 2]));

                            p[x * 3 + y * scanline + 2] =
                                (Byte) (one_minus_y * (double) (p1) + fraction_y * (double) (p2));
                        }
                    }
                }
            }

            b.UnlockBits(bmData);
            bSrc.UnlockBits(bmSrc);

            return b;
        }

        public static Image AverageGrayScale(Image image)
        {
            var bitmap = new Bitmap(image);

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                    var grayVal = (color.R + color.G + color.B) / 3;
                    SetValueTGrayArray(grayVal, color);
                    bitmap.SetPixel(i,j, Color.FromArgb(grayVal, grayVal, grayVal));
                }
            }

            return bitmap;
        }

        public static Image MaxGrayScale(Image image)
        {
            var bitmap = new Bitmap(image);

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                    var grayVal = Math.Max(color.R, Math.Max(color.G, color.B));
                    SetValueTGrayArray(grayVal, color);

                    bitmap.SetPixel(i, j, Color.FromArgb(grayVal, grayVal, grayVal));
                }
            }

            return bitmap;
        }

        public static Image CustomGrayScale(Image image)
        {
            var bitmap = new Bitmap(image);

            var conversionFactor = 255 / (Constants.NUMBER_OF_GREY_SHADES - 1);

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    
                    var color = bitmap.GetPixel(i, j);
                    var avgValue = (color.R + color.G + color.B) / 3;
                    var grayVal = (int)(avgValue / conversionFactor + 0.5) * conversionFactor;
                    SetValueTGrayArray(grayVal, color);

                    bitmap.SetPixel(i, j, Color.FromArgb(grayVal, grayVal, grayVal));
                }
            }

            return bitmap;
        }

        public static Image BackFromGrayScale(Image image)
        {
            var bitmap = new Bitmap(image);

            if (GrayPaletteArray == null)
            {
                return bitmap;
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                    bitmap.SetPixel(i, j, GrayPaletteArray[color.R]);
                }
            }

            return bitmap;
        }

        public static Color[] GenerateColor()
        {
            //0 0 0
            //255 0 0
            //255 255 0
            // 0 255 0
            //0 255 255
            //0 0 255
            //255 0 255
            //255 255 255

            var newColors = new Color[256];
            int r = 127;
            int b = 127;
            int g = 127;

            int factor = 1;
            int step = 36;

            for (int i = 0; i < 256; i++)
            {
                if (i <= step)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r += 3;
                    if (r > 255)
                    {
                        r = 255;
                    }
                }
                else if (i <= step * 2)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g += 3;
                    if (g > 255)
                    {
                        g = 255;
                    }
                }
                else if (i <= step * 3)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r -= 3;
                    if (r < 0)
                    {
                        r = 0;
                    }
                }
                else if (i <= step * 4)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    b += 3;
                    if (b > 255)
                    {
                        b = 255;
                    }
                }
                else if (i <= step * 5)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g -= 3;
                    if (g < 0)
                    {
                        g = 0;
                    }
                }
                else if (i <= step * 6)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r += 3;
                    if (r > 255)
                    {
                        r = 255;
                    }
                }
                else if (i <= step * 7)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g += 3;
                    if (g > 255)
                    {
                        g = 255;
                    }
                }
            }

            return newColors;
        }

        public static Image OrderDithering(Image image)
        {

            var b = new Bitmap(image);
            int[] matrix = new int[] {8, 3, 4, 6, 1, 2, 7, 5, 9};

            int count = 0;
            for (int i = 0; i < b.Width; i++)
            {
                for (int j = 0; j < b.Height; j++)
                {
                    var pixel = b.GetPixel(i, j);
                    int pixelval = pixel.R + pixel.G + pixel.B;

                    double newval = Normalize((double) pixelval, 0.0, 1000.0);
                    newval *= 9;

                    var color = Color.Black;
                    color = newval < (double) matrix[count % 3]
                        ? Color.FromArgb(0, 0, 0)
                        : Color.FromArgb(255, 255, 255);
                    count++;
                    b.SetPixel(i, j, color);
                }
            }

            return b;
        }

        public static Image OrderDitheringUnsafe(Bitmap b)
      {
        int[] matrix = new int[] { 8, 3, 4, 6, 1, 2, 7, 5, 9 };

        int count = 0;

        BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
        ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

        int stride = bmData.Stride;
        System.IntPtr Scan0 = bmData.Scan0;

        unsafe
        {
          byte* p = (byte*)(void*)Scan0;

          int nOffset = stride - b.Width * 3;

          for (int y = 0; y < b.Height; ++y)
          {
             for (int x = 0; x < b.Width; ++x)
             {
               int pixelval = p[0] + p[1] + p[2];

               double newval = Normalize((double)pixelval, 0.0, 1000.0);
               newval *= 9;

               var color = Color.Black;
               color = newval < (double)matrix[count % 3] ? Color.FromArgb(0, 0, 0) : Color.FromArgb(255, 255, 255);
               count++;

               p[0] = p[1] = p[2] = color.R;

               p += 3;
             }

          p += nOffset;
        }
      }

      b.UnlockBits(bmData);

      return b;

    }

        private static double Normalize(double value, double min, double max)
        {
            return (value - min) / (max - min);
        }

        private static Color Find_closest_palette_color(Color pixel)
        {
            return (0.2126 * pixel.R + 0.7152 * pixel.G + 0.0722 * pixel.B) > 128 ? Color.FromArgb(255, 255, 255) : Color.FromArgb(0, 0, 0);
        }

        private static byte GetLimitedValue(byte original, int error)
        {
            int newValue = original + error;
            return (byte)Clamp(newValue, byte.MinValue, byte.MaxValue);
        }

        private static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        public static Color GetQuantError(Color originalPixel, Color newPixel)
        {
            short[] returnValue = new short[4];

            returnValue[0] = (short)(originalPixel.R - newPixel.R);
            returnValue[1] = (short)(originalPixel.G - newPixel.G);
            returnValue[2] = (short)(originalPixel.B - newPixel.B);
            returnValue[3] = (short)(originalPixel.A - newPixel.A);

            returnValue[0] = returnValue[0] < 0 ? (short)0 : returnValue[0];
            returnValue[0] = returnValue[0] > 255 ? (short)255 : returnValue[0];
            returnValue[1] = returnValue[1] < 0 ? (short)0 : returnValue[1];
            returnValue[1] = returnValue[1] > 255 ? (short)255 : returnValue[1];
            returnValue[2] = returnValue[2] < 0 ? (short)0 : returnValue[2];
            returnValue[2] = returnValue[2] > 255 ? (short)255 : returnValue[2];
            returnValue[3] = returnValue[3] < 0 ? (short)0 : returnValue[3];
            returnValue[3] = returnValue[3] > 255 ? (short)255 : returnValue[3];

            return Color.FromArgb(returnValue[3], returnValue[0], returnValue[1], returnValue[2]);
        }

        public static bool IsValidCoordinate(int x, int y, int width, int height)
        {
            return (0 <= x && x < width && 0 <= y && y < height);
        }

        private static void ModifyImageWithErrorAndMultiplier(int x, int y, Color quantError, float multiplier, Bitmap b)
        {
            Color oldColor = Color.White;
            oldColor = b.GetPixel(x, y);

            Color newColor = Color.FromArgb(
              GetLimitedValue(oldColor.R, (int)Math.Round(quantError.R * multiplier)),
              GetLimitedValue(oldColor.G, (int)Math.Round(quantError.G * multiplier)),
              GetLimitedValue(oldColor.B, (int)Math.Round(quantError.B * multiplier)));

            b.SetPixel(x, y, newColor);

        }

        public static Image BurkesDithering(Image image)
        {
            var b= new Bitmap(image);
            Color quantError = Color.Black;

            for (int i = 0; i < b.Width; i++)
            {
                for (int j = 0; j < b.Height; j++)
                {
                    int xMinusOne = i - 1;
                    int xMinusTwo = i - 2;
                    int xPlusOne = i + 1;
                    int xPlusTwo = i + 2;
                    int yPlusOne = j + 1;

                    var pixel = b.GetPixel(i, j);
                    var newPixel = Find_closest_palette_color(pixel);
                    quantError = GetQuantError(pixel, newPixel);

                    int currentRow = j;
                    if (IsValidCoordinate(xPlusOne, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(xPlusOne, currentRow, quantError, 8.0f / 32.0f, b);
                    }

                    if (IsValidCoordinate(xPlusTwo, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(xPlusTwo, currentRow, quantError, 4.0f / 32.0f, b);
                    }
                    currentRow = yPlusOne;
                    if (IsValidCoordinate(xMinusTwo, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(xMinusTwo, currentRow, quantError, 2.0f / 32.0f, b);
                    }

                    if (IsValidCoordinate(xMinusOne, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(xMinusOne, currentRow, quantError, 4.0f / 32.0f, b);
                    }

                    if (IsValidCoordinate(i, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(i, currentRow, quantError, 8.0f / 32.0f, b);
                    }

                    if (IsValidCoordinate(xPlusOne, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(xPlusOne, currentRow, quantError, 4.0f / 32.0f, b);
                    }

                    if (IsValidCoordinate(xPlusTwo, currentRow, b.Width, b.Height))
                    {
                        ModifyImageWithErrorAndMultiplier(xPlusTwo, currentRow, quantError, 2.0f / 32.0f, b);
                    }
                }

            }

            return b;

        }

        public static Image CrossDomainColorize(Image image, int newHue, double newSaturation)
        {
            var b = new Bitmap(image);
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            double h = 0.0, s = 0.0, v = 0.0;
            Color c, cR;
            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width * 3;
                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < nWidth; x += 3)
                    {
                        c = Color.FromArgb((int)p[2], (int)p[1], (int)p[0]);
                        RGBtoHSV(c.R, c.G, c.B, ref h, ref s, ref v);
                        h = newHue;
                        h = h < 0 ? 0 : h;
                        h = h > 360 ? 360 : h;
                        s = s < 0 ? 0 : s;
                        s = s > 100 ? 100 : s;
                        v = v < 0 ? 0 : v;
                        v = v > 255 ? 255 : v;
                        newSaturation = newSaturation > 100 ? 100 : newSaturation;

                        if (newSaturation >= 0)
                            cR = Color.FromArgb((int)h, (int)newSaturation, (int)v);
                        else
                            cR = Color.FromArgb((int)h, (int)s, (int)v);

                        p[0] = cR.B;
                        p[1] = cR.G;
                        p[2] = cR.R;
                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);
            return b;

        }

        private static void RGBtoHSV(double r, double g, double b, ref double h, ref double s, ref double v)
        {
            unsafe
            {
                double max = r > g ? r : g;
                max = max > b ? max : b;
                double min = r < g ? r : g;
                min = min < b ? min : b;

                double delta = max - min;

                if (delta > 0)
                {
                    if (max == r)
                    {
                        h = 60 * (((g - b) / delta) % 6);
                    }
                    else if (max == g)
                    {
                        h = 60 * (((b - r) / delta) + 2);
                    }
                    else if (max == b)
                    {
                        h = 60 * (((r - g) / delta) + 4);
                    }

                    if (max > 0)
                    {
                        s = delta / max;
                    }
                    else
                    {
                        s = 0;
                    }

                    v = max;
                }
                else
                {
                    h = 0;
                    s = 0;
                    v = max;
                }

                if (h < 0)
                {
                    h = 360 + h;
                }
            }
        }
    }
}
