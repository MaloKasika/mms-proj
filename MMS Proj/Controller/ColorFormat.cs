﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS_Proj.Model;

namespace MMS_Proj.Controller
{
    public class ColorFormat
    {
        float[,] RGBToCIE;

        public ColorFormat()
        {
            RGBToCIE = new float[3, 3];
            RGBToCIE[0, 0] = 0.4124564f;
            RGBToCIE[0, 1] = 0.3575761f;
            RGBToCIE[0, 2] = 0.1804375f;
            RGBToCIE[1, 0] = 0.2126729f;
            RGBToCIE[1, 1] = 0.7151522f;
            RGBToCIE[1, 2] = 0.0721750f;
            RGBToCIE[2, 0] = 0.0193339f;
            RGBToCIE[2, 1] = 0.1191920f;
            RGBToCIE[2, 2] = 0.9503041f;
        }

        public CIEColorFormat ConvertRGBColorToCIEValues(Color toTransform)
        {
            float[] newValues = new float[3];
            int[] oldValues = new int[] { toTransform.R, toTransform.G, toTransform.B };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    newValues[i] += RGBToCIE[i, j] * oldValues[j];
                }
            }
            
            return new CIEColorFormat{ CComponent = (int)newValues[0], EComponent = (int)newValues[1], IComponent = (int)newValues[2]};
        }

        public CIEValueList GetCIEValuesFromImage(Image image)
        {
            CIEValueList listToReturn = new CIEValueList();

            var bitmap = new Bitmap(image);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    listToReturn.ListOfColors.Add(ConvertRGBColorToCIEValues(bitmap.GetPixel(i,j)));
                }
            }

            return listToReturn;
        }

        public Color ConvertRGBColorToCIEColor(Color toTransform)
        {
            float[] newValues=new float[3];
            int[] oldValues = new int[] {toTransform.R, toTransform.G, toTransform.B};
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    newValues[i] += RGBToCIE[i, j] * oldValues[j];
                }

                if (newValues[i] > 255)
                {
                    newValues[i] = 255;
                }

                if (newValues[i] < 0)
                {
                    newValues[i] = 0;
                }
            }


            return Color.FromArgb((int)newValues[0], (int)newValues[1], (int)newValues[2]);
        }

        public Image GetRGBComponetns(Image image)
        {

            var bitmap = new Bitmap(image);

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    var newColor = ConvertRGBColorToCIEColor(pix);
                    bitmap.SetPixel(i, j, newColor);
                }
            }

            return bitmap;
        }

        public Image GetRChanal(Image image)
        {

            var bitmap = new Bitmap(image);

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    var newColor = ConvertRGBColorToCIEColor(pix);
                    bitmap.SetPixel(i, j, Color.FromArgb(255, newColor.G, newColor.B));
                }
            }

            return bitmap;
        }

        public Image GetGChanal(Image image)
        {
            var bitmap = new Bitmap(image);

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    var newColor = ConvertRGBColorToCIEColor(pix);

                    bitmap.SetPixel(i, j, Color.FromArgb(newColor.R, 255, newColor.B));
                }
            }

            return bitmap;
        }

        public Image GetBChanal(Image image)
        {

            var bitmap = new Bitmap(image);

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    var newColor = ConvertRGBColorToCIEColor(pix);
                    bitmap.SetPixel(i, j, Color.FromArgb(newColor.R, newColor.G, 255));
                }
            }

            return bitmap;
        }

        public Image ConvertRBGToCIEUnsafe(Image image)
        {
            var bitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - bitmap.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < bitmap.Height; ++y)
                {
                    for (int x = 0; x < bitmap.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        var color = ConvertRGBColorToCIEColor(Color.FromArgb(red, green, blue));

                        p[0] = color.B;
                        p[1] = color.G;
                        p[2] = color.R;

                        p += 3;
                    }

                    p += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);

            return bitmap;
        }

        public Image GetRChanalUnsafe(Image image)
        {
            var bitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bitmap.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < bitmap.Height; ++y)
                {
                    for (int x = 0; x < bitmap.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        var color = ConvertRGBColorToCIEColor(Color.FromArgb(red, green, blue));

                        p[0] = color.B;
                        p[1] = color.G;
                        p[2] = 255;

                        p += 3;
                    }

                    p += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);

            return bitmap;
        }
        public Image GetGChanalUnsafe(Image image)
        {
            var bitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bitmap.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < bitmap.Height; ++y)
                {
                    for (int x = 0; x < bitmap.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        var color = ConvertRGBColorToCIEColor(Color.FromArgb(red, green, blue));

                        p[0] = color.B;
                        p[1] = 255;
                        p[2] = color.R;

                        p += 3;
                    }

                    p += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);

            return bitmap;
        }
        public Image GetBChanalUnsafe(Image image)
        {
            var bitmap = new Bitmap(image);

            BitmapData bmData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bitmap.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < bitmap.Height; ++y)
                {
                    for (int x = 0; x < bitmap.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        var color = ConvertRGBColorToCIEColor(Color.FromArgb(red, green, blue));

                        p[0] = 255;
                        p[1] = color.G;
                        p[2] = color.R;

                        p += 3;
                    }

                    p += nOffset;
                }
            }

            bitmap.UnlockBits(bmData);

            return bitmap;
        }

    }
}
