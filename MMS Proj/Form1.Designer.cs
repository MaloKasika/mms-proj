﻿namespace MMS_Proj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewPanels = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadPhotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePhotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meanRemovalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeWarpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorGrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderDiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.burkesDitheringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoBtn = new System.Windows.Forms.Button();
            this.redoBtn = new System.Windows.Forms.Button();
            this.unsafeCbx = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // viewPanels
            // 
            this.viewPanels.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.viewPanels.Location = new System.Drawing.Point(12, 27);
            this.viewPanels.Name = "viewPanels";
            this.viewPanels.Size = new System.Drawing.Size(386, 344);
            this.viewPanels.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.filtersToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(410, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadPhotoToolStripMenuItem,
            this.savePhotoToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // loadPhotoToolStripMenuItem
            // 
            this.loadPhotoToolStripMenuItem.Name = "loadPhotoToolStripMenuItem";
            this.loadPhotoToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.loadPhotoToolStripMenuItem.Text = "Load photo";
            this.loadPhotoToolStripMenuItem.Click += new System.EventHandler(this.loadPhotoToolStripMenuItem_Click);
            // 
            // savePhotoToolStripMenuItem
            // 
            this.savePhotoToolStripMenuItem.Name = "savePhotoToolStripMenuItem";
            this.savePhotoToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.savePhotoToolStripMenuItem.Text = "Save photo";
            this.savePhotoToolStripMenuItem.Click += new System.EventHandler(this.savePhotoToolStripMenuItem_Click);
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.channelsToolStripMenuItem,
            this.colorToolStripMenuItem,
            this.invertToolStripMenuItem,
            this.meanRemovalToolStripMenuItem,
            this.edgeDetectionToolStripMenuItem,
            this.timeWarpToolStripMenuItem,
            this.histogramToolStripMenuItem,
            this.grayScaleToolStripMenuItem,
            this.colorGrayToolStripMenuItem,
            this.orderDiToolStripMenuItem,
            this.burkesDitheringToolStripMenuItem,
            this.crrosToolStripMenuItem});
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.filtersToolStripMenuItem.Text = "Filters";
            // 
            // channelsToolStripMenuItem
            // 
            this.channelsToolStripMenuItem.Name = "channelsToolStripMenuItem";
            this.channelsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.channelsToolStripMenuItem.Text = "Channels";
            this.channelsToolStripMenuItem.Click += new System.EventHandler(this.channelsToolStripMenuItem_Click);
            // 
            // colorToolStripMenuItem
            // 
            this.colorToolStripMenuItem.Name = "colorToolStripMenuItem";
            this.colorToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.colorToolStripMenuItem.Text = "Color";
            this.colorToolStripMenuItem.Click += new System.EventHandler(this.colorToolStripMenuItem_Click);
            // 
            // invertToolStripMenuItem
            // 
            this.invertToolStripMenuItem.Name = "invertToolStripMenuItem";
            this.invertToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.invertToolStripMenuItem.Text = "Invert";
            this.invertToolStripMenuItem.Click += new System.EventHandler(this.invertToolStripMenuItem_Click);
            // 
            // meanRemovalToolStripMenuItem
            // 
            this.meanRemovalToolStripMenuItem.Name = "meanRemovalToolStripMenuItem";
            this.meanRemovalToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.meanRemovalToolStripMenuItem.Text = "Mean removal";
            this.meanRemovalToolStripMenuItem.Click += new System.EventHandler(this.meanRemovalToolStripMenuItem_Click);
            // 
            // edgeDetectionToolStripMenuItem
            // 
            this.edgeDetectionToolStripMenuItem.Name = "edgeDetectionToolStripMenuItem";
            this.edgeDetectionToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.edgeDetectionToolStripMenuItem.Text = "Edge detect homogenity";
            this.edgeDetectionToolStripMenuItem.Click += new System.EventHandler(this.edgeDetectionToolStripMenuItem_Click);
            // 
            // timeWarpToolStripMenuItem
            // 
            this.timeWarpToolStripMenuItem.Name = "timeWarpToolStripMenuItem";
            this.timeWarpToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.timeWarpToolStripMenuItem.Text = "Time Warp";
            this.timeWarpToolStripMenuItem.Click += new System.EventHandler(this.timeWarpToolStripMenuItem_Click);
            // 
            // histogramToolStripMenuItem
            // 
            this.histogramToolStripMenuItem.Name = "histogramToolStripMenuItem";
            this.histogramToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.histogramToolStripMenuItem.Text = "Histogram";
            this.histogramToolStripMenuItem.Click += new System.EventHandler(this.histogramToolStripMenuItem_Click);
            // 
            // grayScaleToolStripMenuItem
            // 
            this.grayScaleToolStripMenuItem.Name = "grayScaleToolStripMenuItem";
            this.grayScaleToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.grayScaleToolStripMenuItem.Text = "Gray Scale";
            this.grayScaleToolStripMenuItem.Click += new System.EventHandler(this.grayScaleToolStripMenuItem_Click);
            // 
            // colorGrayToolStripMenuItem
            // 
            this.colorGrayToolStripMenuItem.Name = "colorGrayToolStripMenuItem";
            this.colorGrayToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.colorGrayToolStripMenuItem.Text = "Color Gray";
            this.colorGrayToolStripMenuItem.Click += new System.EventHandler(this.colorGrayToolStripMenuItem_Click);
            // 
            // orderDiToolStripMenuItem
            // 
            this.orderDiToolStripMenuItem.Name = "orderDiToolStripMenuItem";
            this.orderDiToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.orderDiToolStripMenuItem.Text = "Order Dithering";
            this.orderDiToolStripMenuItem.Click += new System.EventHandler(this.orderDiToolStripMenuItem_Click);
            // 
            // burkesDitheringToolStripMenuItem
            // 
            this.burkesDitheringToolStripMenuItem.Name = "burkesDitheringToolStripMenuItem";
            this.burkesDitheringToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.burkesDitheringToolStripMenuItem.Text = "Burkes Dithering";
            this.burkesDitheringToolStripMenuItem.Click += new System.EventHandler(this.burkesDitheringToolStripMenuItem_Click);
            // 
            // crrosToolStripMenuItem
            // 
            this.crrosToolStripMenuItem.Name = "crrosToolStripMenuItem";
            this.crrosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.crrosToolStripMenuItem.Text = "Cross Domain";
            this.crrosToolStripMenuItem.Click += new System.EventHandler(this.crrosToolStripMenuItem_Click);
            // 
            // undoBtn
            // 
            this.undoBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.undoBtn.Location = new System.Drawing.Point(352, 4);
            this.undoBtn.Name = "undoBtn";
            this.undoBtn.Size = new System.Drawing.Size(20, 20);
            this.undoBtn.TabIndex = 3;
            this.undoBtn.Text = "<";
            this.undoBtn.UseVisualStyleBackColor = true;
            this.undoBtn.Click += new System.EventHandler(this.undoBtn_Click);
            // 
            // redoBtn
            // 
            this.redoBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.redoBtn.Location = new System.Drawing.Point(378, 4);
            this.redoBtn.Name = "redoBtn";
            this.redoBtn.Size = new System.Drawing.Size(20, 20);
            this.redoBtn.TabIndex = 4;
            this.redoBtn.Text = ">";
            this.redoBtn.UseVisualStyleBackColor = true;
            this.redoBtn.Click += new System.EventHandler(this.redoBtn_Click);
            // 
            // unsafeCbx
            // 
            this.unsafeCbx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.unsafeCbx.AutoSize = true;
            this.unsafeCbx.Location = new System.Drawing.Point(257, 7);
            this.unsafeCbx.Name = "unsafeCbx";
            this.unsafeCbx.Size = new System.Drawing.Size(89, 17);
            this.unsafeCbx.TabIndex = 1;
            this.unsafeCbx.Text = "Unsafe mode";
            this.unsafeCbx.UseVisualStyleBackColor = true;
            this.unsafeCbx.CheckedChanged += new System.EventHandler(this.unsafeCbx_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 383);
            this.Controls.Add(this.redoBtn);
            this.Controls.Add(this.unsafeCbx);
            this.Controls.Add(this.undoBtn);
            this.Controls.Add(this.viewPanels);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(358, 422);
            this.Name = "Form1";
            this.Text = "Instagram";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel viewPanels;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadPhotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePhotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelsToolStripMenuItem;
        private System.Windows.Forms.Button undoBtn;
        private System.Windows.Forms.Button redoBtn;
        private System.Windows.Forms.CheckBox unsafeCbx;
        private System.Windows.Forms.ToolStripMenuItem meanRemovalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timeWarpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem histogramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayScaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorGrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderDiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem burkesDitheringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crrosToolStripMenuItem;
    }
}

