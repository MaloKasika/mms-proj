﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MMS_Proj.Controller;
using MMS_Proj.Helpers;
using MMS_Proj.Model;
using MMS_Proj.View;

namespace MMS_Proj
{
    public partial class Form1 : Form
    {
        SingleSeason singlePic;
        FourSeasons multiPic;
        Histogram histo;
        private Image mainImage;
     
        public Form1()
        {
            InitializeComponent();
            singlePic = new SingleSeason();
            multiPic = new FourSeasons();
            histo = new Histogram();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Global.ProgramState = StateType.SafeState;
            singlePic.Name = "picFrame";
            singlePic.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            viewPanels.Controls.Clear();
            viewPanels.Size = singlePic.Size;
            this.Size = new Size(viewPanels.Width + 20 , viewPanels.Height+80);
            viewPanels.Controls.Add(singlePic);
            
        }

        private void loadPhotoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog
            {
                Filter = "jpg files (*.jpg)|*.jpg| bmp files (*.bmp)|*.bmp| png files (*.png)|*.png"
            };


            if (fileDialog.ShowDialog() != DialogResult.OK) return;

            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            mainImage = Image.FromFile(fileDialog.FileName);
            photoFrame.SetMainPhoto(mainImage);
     
            UndoRedo.ClearStacks();
        }

        private void savePhotoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileDialog = new SaveFileDialog()
            {
                Filter = "jpg files (*.jpg)|*.jpg| bmp files (*.bmp)|*.bmp| png files (*.png)|*.png",
                RestoreDirectory = true
            };


            var format = ImageFormat.Jpeg;
            if (fileDialog.ShowDialog() != DialogResult.OK) return;

            switch (Path.GetExtension(fileDialog.FileName))
            {
                case ".jpg":
                    format = ImageFormat.Jpeg;
                    break;
                case ".png":
                    format = ImageFormat.Png;
                    break;
                case ".bmp":
                    format = ImageFormat.Bmp;
                    break;
            }

            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var photoToSave = photoFrame?.GetMainImage();
            photoToSave?.Save(fileDialog.FileName, format);
        }

        private void channelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (singlePic.Picture.Image != null)
            {
                multiPic.SetMainPhoto(singlePic.Picture.Image);
            }

            multiPic.Name = "picFrame";
            multiPic.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            viewPanels.Controls.Clear();
            viewPanels.Size = new Size(multiPic.Width + 40, multiPic.Height + 80);
            this.Size = viewPanels.Size;
            viewPanels.Controls.Add(multiPic);
            var action = new ActionMMS {ActionType = ActionType.Transition};
            action.DoneFuntion += ChangeViews;
          
            UndoRedo.PushToStack(action);

        }

        private void histogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (singlePic.Picture.Image != null)
            {
                histo.SetMainPhoto(singlePic.GetMainImage());
            }

            histo.Name = "picFrame";
            histo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            viewPanels.Controls.Clear();
            viewPanels.Size = new Size(histo.Width + 40, histo.Height + 80);
            this.Size = viewPanels.Size;
            viewPanels.Controls.Add(histo);
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.ColorFilter);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.ColorFilter); };
            UndoRedo.PushToStack(action);
        }

        private void invertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.InvertFilter);

            var action = new ActionMMS() { ActionType = ActionType.Filter};
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.InvertFilter); };
            UndoRedo.PushToStack(action);
        }

        private void meanRemovalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.MeanRemoval);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.MeanRemoval); };
            UndoRedo.PushToStack(action);

        }

        private void edgeDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.EdgeDetectHomogenity);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.EdgeDetectHomogenity); };
            UndoRedo.PushToStack(action);

        }

        private void timeWarpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.TimeWarp);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.TimeWarp); };
            UndoRedo.PushToStack(action);

        }

        private void grayScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.GrayScale);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.GrayScale); };
            UndoRedo.PushToStack(action);

        }

        private void colorGrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.BackFromGray);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.BackFromGray); };
            UndoRedo.PushToStack(action);

        }

        private void orderDiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.OrderDithering);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.OrderDithering); };
            UndoRedo.PushToStack(action);

        }

        private void burkesDitheringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.BurkesDithering);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.BurkesDithering); };
            UndoRedo.PushToStack(action);

        }

        private void crrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault() as IPhotoFrame;
            var oldImage = photoFrame?.GetMainImage();
            photoFrame?.FilterImage(FilterType.CrossDomain);

            var action = new ActionMMS() { ActionType = ActionType.Filter };
            action.OldImage += (o, args) => { photoFrame?.SetMainPhoto(oldImage); };
            action.DoneFuntion += (o, args) => { photoFrame?.FilterImage(FilterType.CrossDomain); };
            UndoRedo.PushToStack(action);

        }

        private void ChangeViews(object obj, EventArgs e)
        {
            var photoFrame = viewPanels.Controls.Find("picFrame", true).FirstOrDefault();

            if (photoFrame is SingleSeason)
            {
                if (singlePic.Picture.Image != null)
                {
                    multiPic.SetMainPhoto(singlePic.Picture.Image);
                }

                viewPanels.Controls.Clear();
                viewPanels.Size = new Size(multiPic.Width + 40, multiPic.Height + 80);
                this.Size = viewPanels.Size;
                viewPanels.Controls.Add(multiPic);
            }
            else if (photoFrame is FourSeasons)
            {
                viewPanels.Controls.Clear();
                viewPanels.Size = singlePic.Size;
                this.Size = new Size(viewPanels.Width + 20, viewPanels.Height + 80);
                viewPanels.Controls.Add(singlePic);

            }

        }

        private void undoBtn_Click(object sender, EventArgs e)
        {
            UndoRedo.Undo();
        }

        private void redoBtn_Click(object sender, EventArgs e)
        {
            UndoRedo.Redo();
        }

        private void unsafeCbx_CheckedChanged(object sender, EventArgs e)
        {
            Global.ProgramState = (Global.ProgramState == StateType.SafeState)
                ? StateType.UnsafeState
                : StateType.SafeState;
        }

    }
}
